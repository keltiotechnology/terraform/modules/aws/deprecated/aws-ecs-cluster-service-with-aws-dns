resource "aws_route53_record" "service_record" {
  zone_id         = var.hosted_zone_id
  name            = var.app_fqdn
  type            = "CNAME"
  ttl             = 60
  records         = ["${data.aws_cloudfront_distribution.entrypoint.domain_name}."]
  allow_overwrite = true
}

resource "aws_route53_record" "www_service_record" {
  zone_id         = var.hosted_zone_id
  name            = "www.${var.app_fqdn}"
  type            = "CNAME"
  ttl             = 60
  records         = ["${data.aws_cloudfront_distribution.entrypoint.domain_name}."]
  allow_overwrite = true
}
